#!/usr/bin/env python3

from openai import OpenAI
import pyttsx3
import speech_recognition as sr
import tempfile
import playsound
import os
from apikey import *


def main():
    if not API_KEY:
        raise Exception("API key is not set")

    if not ORGANIZATION:
        raise Exception("Organization is not set")

    client = OpenAI(
        api_key=API_KEY,
        organization=ORGANIZATION,
    )

    # Text-to-speech engine
    engine = pyttsx3.init()

    while True:
        """
        Listen for audio input, recognize it and respond using OpenAI
        """

        # Create speech recognizer object
        r = sr.Recognizer()

        # Listen for input
        with sr.Microphone() as source:
            print("Listening...")
            audio = r.listen(source)

        # Try to recognize the audio
        try:
            prompt = r.recognize_google(audio, language="en-EN", show_all=False)
            print("You asked:", prompt)

            # Use OpenAI to create a response
            response = client.chat.completions.create(
                model="gpt-4",
                messages=[{"role": "user", "content": prompt}],
            )

            # Get the response text
            response_text = str(response.choices[0].message.content).strip("\n")
            print("Response: ", response_text)

            # Speak the response
            with tempfile.TemporaryDirectory() as directory_name:
                speech_file_path = os.path.join(directory_name, "openai-demo.mp3")
                response = client.audio.speech.create(
                    model="tts-1",
                    voice="alloy",
                    input=response_text,
                )

                response.write_to_file(speech_file_path)

                playsound.playsound(speech_file_path)

            print()

        # Catch if recognition fails
        except sr.UnknownValueError:
            response_text = "Sorry, I didn't understand what you said"
            print(response_text)
            engine.say(response_text)
            engine.runAndWait()
            print()
        except sr.RequestError as e:
            print(
                "Could not request results from Google Speech Recognition service; {0}".format(
                    e
                )
            )


if __name__ == "__main__":
    main()
