# openAI demos

All code is written in `python3`. To set up the environment use these steps:

```bash
sudo apt install python3.10-venv python3.10-dev

sudo apt-get install portaudio19-dev python3-pyaudio
sudo apt install espeak libespeak-dev

# set up the virtual environment
python3 -m venv ./venv

# activate the venv
source ./venv/bin/activate

# install needed libraries
pip install -r requirements.txt
```

Then create a file called `apikey.py` in this folder with 2 lines of content:

```txt
API_KEY = "YOUR-API-KEY"
ORGANIZATION = "YOUR-ORGANIZATION"
```

## Conversation with chatgpt

To have a continuous conversation with chatGPT turn on your speakers, connect a microphone and run

```bash
python3 ./main.py
```
